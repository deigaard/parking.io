var http = require('http');
var fs = require('fs');
var request = require("request");

// Loading the file index.html displayed to the client
var server = http.createServer(function(req, res) {
    fs.readFile('./camera-presets.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});

// Loading socket.io
var io = require('socket.io').listen(server);

io.sockets.on('connection', function (socket, username) {
    // When the client connects, they are sent a message
    socket.emit('message', 'You are now connected to the Camera Control System.');
    // The other clients are told that someone new has arrived
//    socket.broadcast.emit('message', 'Another client has just connected!');

    // As soon as the username is received, it's stored as a session variable
    socket.on('little_newbie', function(username) {
        socket.username = username;
    });

    // When a "message" is received (click on the button), it's logged in the console
    socket.on('message', function (message) {
        // The username of the person who clicked is retrieved from the session variables
        console.log(socket.username + ' is speaking to me! They\'re saying: ' + message);
	var host = "http://10.64.181.31:";
	var url = "/axis-cgi/com/ptz.cgi?gotoserverpresetno=";
	var portpreset = message.split(',').map(n => parseInt(n, 10) );
		console.log(portpreset);
	var port = portpreset[0];
	var preset = portpreset[1];
	var call = host + port + url + preset;
		console.log(call);
	request(call, function(error,response,body) {
		//console.log(body);
	});
    }); 
});


server.listen(8080);
